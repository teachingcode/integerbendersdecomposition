/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package problems;

/**
 *
 * @author lct495
 */
public class FacilityLocationProblem {
    private final int nFacilities;
    private final int nCustomers;
    private final double openingCosts[];
    private final double deliveryCosts[][];
    private final double demands[];
    private final double capacity[];

    public FacilityLocationProblem(int nFacilities, int nCustomers, double[] openingCosts, double[][] deliveryCosts, double[] demands, double[] capacity) {
        this.nFacilities = nFacilities;
        this.nCustomers = nCustomers;
        this.openingCosts = openingCosts;
        this.deliveryCosts = deliveryCosts;
        this.demands = demands;
        this.capacity = capacity;
    }

    public int getnFacilities() {
        return nFacilities;
    }

    public int getnCustomers() {
        return nCustomers;
    }

    public double[] getOpeningCosts() {
        return openingCosts;
    }

    public double[][] getDeliveryCosts() {
        return deliveryCosts;
    }

    public double[] getDemands() {
        return demands;
    }

    public double[] getCapacity() {
        return capacity;
    }
    
    
    
    
}
