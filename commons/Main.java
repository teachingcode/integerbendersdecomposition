/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package commons;
import problems.FacilityLocationProblem;
import benders.MasterProblem;
import fullmodels.FullModel;
import ilog.concert.IloException;

/**
 *
 * @author lct495
 */
public class Main {
    
    public static void main(String[] args) throws IloException{
        // Populates the data of the problem
        int nFacilities = 4;
        int nCustomers = 4;
        
        double openingCosts[] = new double[nFacilities];
        openingCosts[0] = 67;
        openingCosts[1] = 72;
        openingCosts[2] = 63;
        openingCosts[3] = 52;
        // Populates a matrix of delivery costs
        double deliveryCosts[][] = new double[nFacilities][nCustomers];
        deliveryCosts[0][0] = 10;
        deliveryCosts[0][1] = 10;
        deliveryCosts[0][2] = 10;
        deliveryCosts[0][3] = 10;
        deliveryCosts[1][0] = 9;
        deliveryCosts[1][1] = 8;
        deliveryCosts[1][2] = 7;
        deliveryCosts[1][3] = 11;
        deliveryCosts[2][0] = 11;
        deliveryCosts[2][1] = 6;
        deliveryCosts[2][2] = 5;
        deliveryCosts[2][3] = 10;
        deliveryCosts[3][0] = 15;
        deliveryCosts[3][1] = 16;
        deliveryCosts[3][2] = 9;
        deliveryCosts[3][3] = 7;
        
        // Pupulates the array of demands
        double demands[] = new double[nCustomers];
        demands[0] = 10;
        demands[1] = 19;
        demands[2] = 21;
        demands[3] = 30;
        
        // Populates the array of upper bounds on the capacities
        double capacity[] = new double[nFacilities];
        capacity[0] = 55;
        capacity[1] = 23;
        capacity[2] = 32;
        capacity[2] = 52;
        
        // Creates an instance of the capacity planning problem
        FacilityLocationProblem cpp = new FacilityLocationProblem(nFacilities, nCustomers, 
                openingCosts, deliveryCosts, demands, capacity);
        
        // Creates the Master Problem
        MasterProblem mp = new MasterProblem(cpp);
        mp.solve();
        System.out.println("Optimal Benders objective value = "+mp.getObjective());
        mp.printSolution();
       
        // Checks if the results are consistent with solving the full problem
        System.out.println("=======================");
        System.out.println("SOLUTION WITHOUT DECOMPOSITION");
        System.out.println("=======================");
        FullModel m = new FullModel(cpp);
        m.solve();
        
    }
    
}
