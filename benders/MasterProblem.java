/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.FacilityLocationProblem;

/**
 *
 * @author lct495
 */
public class MasterProblem {
    
    private final IloCplex model;
    private final IloIntVar x[];
    private final IloNumVar phi;
    private final FacilityLocationProblem flp;

    /**
     * Creates the Master Problem.
     * @param flp
     * @throws IloException 
     */
    public MasterProblem(FacilityLocationProblem flp) throws IloException {
        this.flp = flp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        this.x = new IloIntVar[flp.getnFacilities()];
        
        for(int i = 1; i<= flp.getnFacilities(); i++){
            x[i-1] = model.boolVar();
        }
        this.phi = model.numVar(0, Double.POSITIVE_INFINITY,"phi");
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        for(int i = 1; i <= flp.getnFacilities(); i++){
            obj.addTerm(flp.getOpeningCosts()[i-1], x[i-1]);
        }
        obj.addTerm(1, phi);
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        
    }
    /**
     * Solves the Master Problem.
     * @throws IloException 
     */
    public void solve() throws IloException{
        
        // In this way we inform Cplex that
        // we want to use the callback we define below
        model.use(new MyCallback());
        
        // Solves the problem
        model.solve();
    }
    
    /**
     * The class Callback extends the LazyConstraintCallback. 
     * This means that objects of the class Callback must have a 
     * main() method. When we tell the model to use this callback
     * (model.use(newCallback()); this method called by the model 
     * every time the branch and bound algorithm reaches an integer node.
     * Therefore, within the main method we code all the actions
     * that must be performed upon reaching integer nodes,
     * that is all the actions to verify optimality and 
     * possibly add feasibility or optimality cuts. 
     * Note that "Callback" is an arbitrary name.
     * See more information about the LazyConstraintCallback
     * here https://www.ibm.com/support/knowledgecenter/SSSA5P_12.6.3/ilog.odms.cplex.help/refjavacplex/html/ilog/cplex/IloCplex.LazyConstraintCallback.html 
     */
    private class MyCallback extends IloCplex.LazyConstraintCallback{

        public MyCallback() {
        }
        /**
         * This is the main method of the Callback class.
         * Whatever we code in this method will be run every time
         * the B&B method reaches an integer node. Here we 
         * code the routines necessary to verify whether we need
         * cuts and, in case, generate and add cuts. 
         * @throws IloException 
         */
        @Override
        protected void main() throws IloException {
            // 1. We start by obtaining the solution at the current node
            double[] X = getX();
            double Phi = getPhi();
            
            // 2. We check feasibility of the subproblem 
            // 2.1 We create and solve a feasibility subproblem 
            FeasibilitySubProblem fsp = new FeasibilitySubProblem(flp,X);
            fsp.solve();
            double fspObjective = fsp.getObjective();
            
            // 2.2 We check if the suproblem is feasible. 
            // Remember, if the objective is zero the subproblem is feasible
            System.out.println("FSP "+fspObjective);
            if(fspObjective >= 0+1e-9){
                // 2.3 If the objective is positive 
                // the subproblem is not feasible. Thus we 
                // need a feasibility cut.
                System.out.println("Generating feasibility cut");
                // 2.4 We obtain the constant and the linear term of the cut
                // from the feasibility subproblem
                double constant = fsp.getCutConstant();
                IloLinearNumExpr linearTerm = fsp.getCutLinearTerm(x);
                
                // 2.5 Thus we generate and add a cut to the current model.
                // Remember that the cut is constant + linearTerm <= 0.
                // Notice that we use the method add() from the LazyConstraintCallback
                // class. This method adds the cut "lazily" to the model being
                // solved. Instead, the method model.le() does not add a cut!
                // It only creates and returns an IloRange object (which models 
                // a constraint. Notice the difference between model.le()
                // and model.addLe() which we used when creating the model.
                IloRange cut = model.le(linearTerm, -constant);
                add(cut);
            }else{
                // 3. Since the subproblem is feasible, we check optimality
                // and verify whether we should add an optimality cut.
                
                // 3.1. First, we create and solve an optimality suproblem
                OptimalitySubProblem osp = new OptimalitySubProblem(flp,X);
                osp.solve();
                double ospObjective = osp.getObjective();
                
                // 3.2. Then we check if the optimality test is satisfied.
                System.out.println("Phi "+Phi+ " OSP "+ospObjective );
                if(Phi >= ospObjective - 1e-9){
                    // 3.3. In this case the problem at the current node
                    // is optimal.
                    System.out.println("The current node is optimal");
                }else{
                    // 3.4. In this case we need an optimality cut. 
                    System.out.println("Generating optimality cut");
                    // We get the constant and the linear term from
                    // the optimality suproblem 
                    double cutConstant = osp.getCutConstant();
                    IloLinearNumExpr cutTerm = osp.getCutLinearTerm(x);
                    cutTerm.addTerm(-1, phi);
                    // and generate and add a cut.
                    IloRange cut = model.le(cutTerm, -cutConstant);
                    add(cut);
                }
            }
        }
        /**
        * Returns the X component of the solution at the current B&B integer node
        * @return the X component of the current solution
        * @throws IloException 
        */
        public double[] getX() throws IloException{
           double X[] = new double[flp.getnFacilities()];
           for(int i = 1; i<= flp.getnFacilities() ;i++){
               // Note that we do not use model.getValue()
               // but getValue() which is a method inherited 
               // from the LazyConstraintsCallback class
               // and is used to query the solution at the current node
               // and not the solution once the B&B method has 
               // been terminated.
               X[i-1] = getValue(x[i-1]);
           }
           return X;
       }
       /**
        * Returns the value of phi at the current B&B integer node.
        * @return the value of phi.
        * @throws IloException 
        */
        public double getPhi() throws IloException{
           // notice again that we use getValue() and not
           // model.getValue()
           return getValue(phi);
        }
    }
    
    
    /**
     * Returns the objective value
     * @return
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    /**
     * Prints the solution
     * @throws IloException 
     */
    public void printSolution() throws IloException{
        for(int i = 1; i <= flp.getnFacilities(); i++){
            System.out.println("X_"+i+" = "+model.getValue((x[i-1])));
        }
    }   
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
}
