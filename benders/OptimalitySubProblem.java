/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package benders;

import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.concert.IloRange;
import ilog.cplex.IloCplex;
import problems.FacilityLocationProblem;

/**
 *
 * @author lct495
 */
public class OptimalitySubProblem {
    
    private final IloCplex model;
    private final IloNumVar y[][];
    private final FacilityLocationProblem flp;
    private final IloRange capacityConstraints[];
    private final IloRange demandConstraints[];
    /**
     * Creates the LP model for the optimality subproblem
     * @param flp
     * @param X a solution to MP
     * @throws IloException 
     */
    public OptimalitySubProblem(FacilityLocationProblem flp, double X[]) throws IloException {
        this.flp = flp;
        
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        // and we have 5 cities to serve
        y = new IloNumVar[flp.getnFacilities()][flp.getnCustomers()];
        // Assigns to each position in the array an IloNumVar object 
        // i.e., a decisio variable in Cplex's language
        
        for(int i = 1; i<= flp.getnFacilities(); i++){
            for(int j = 1; j <= flp.getnCustomers(); j++){
                y[i-1][j-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        
        for(int i = 1; i<= flp.getnFacilities(); i++){
            for(int j = 1; j <= flp.getnCustomers(); j++){
                obj.addTerm(flp.getDeliveryCosts()[i-1][j-1], y[i-1][j-1]);
            }
        }
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. Creates the constraints
        // For each constraints creates an populates a linear equation
        this.capacityConstraints = new IloRange[flp.getnFacilities()];
        for(int i = 1; i<= flp.getnFacilities(); i++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int j = 1; j <= flp.getnCustomers(); j++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            capacityConstraints[i-1] = model.addLe(lhs, flp.getCapacity()[i-1]*X[i-1]);
        }
        
        this.demandConstraints = new IloRange[flp.getnCustomers()];
        for(int j = 1; j <= flp.getnCustomers(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i = 1; i<= flp.getnFacilities(); i++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            demandConstraints[j-1] = model.addGe(lhs, flp.getDemands()[j-1]);
        }
        
        
    }
    
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
    }
    
    /**
     * Returns the objective value
     * @return the objective value
     * @throws IloException 
     */
    public double getObjective() throws IloException{
        return model.getObjValue();
    }
    
    /**
     * Returns the constant part of the optimality cut.
     * That is, the part of the cut not dependent on x.
     * This is given by the demand constraints. 
     * @return the constant of the cut
     * @throws IloException 
     */
    public double getCutConstant() throws IloException{
        double constant = 0;
        for(int j = 1; j <= flp.getnCustomers(); j++){
            constant = constant + model.getDual(demandConstraints[j-1]) * flp.getDemands()[j-1];
        }
        return constant;
    }
    /**
     * Returns the linear expression in x of the optimality cut.
     * The liner term is obtained from the capacity constraints since they
     * involve x on the RHS. 
     * @param x the x variables of the master problem
     * @return the linear term of the cut
     * @throws IloException 
     */
    public IloLinearNumExpr getCutLinearTerm(IloIntVar x[]) throws IloException{
        IloLinearNumExpr cutTerm = model.linearNumExpr();
        
        for(int i = 1; i <= flp.getnFacilities(); i++){
            cutTerm.addTerm(model.getDual(capacityConstraints[i-1]) * flp.getCapacity()[i-1], x[i-1]); 
        }
        return cutTerm;
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
    
}
