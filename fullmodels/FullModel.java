/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fullmodels;
import problems.FacilityLocationProblem;
import ilog.concert.IloException;
import ilog.concert.IloIntVar;
import ilog.concert.IloLinearNumExpr;
import ilog.concert.IloNumVar;
import ilog.cplex.IloCplex;

/**
 *
 * @author lct495
 */
public class FullModel {
    
    private final IloCplex model;
    private final IloIntVar x[];
    private final IloNumVar y[][];
    private final FacilityLocationProblem flp;
    /**
     * Creates the IP model for the original problem
     * (without decomposition)
     * @param flp
     * @throws IloException 
     */
    public FullModel(FacilityLocationProblem flp) throws IloException {
        this.flp = flp;
        
        // 1. Every model needs an IloCplex object
        this.model = new IloCplex();
        
        // 2. Creates the decision variables
        x = new IloIntVar[flp.getnFacilities()];
        // and we have 5 cities to serve
        y = new IloNumVar[flp.getnFacilities()][flp.getnCustomers()];
        
        for(int i = 1; i<= flp.getnFacilities(); i++){
            x[i-1] = model.boolVar();
        }
        for(int i = 1; i<= flp.getnFacilities(); i++){
            for(int j = 1; j <= flp.getnCustomers(); j++){
                y[i-1][j-1] = model.numVar(0, Double.POSITIVE_INFINITY);
            }
        }
        
        // 3. Creates the objective function
        // Creates an empty linear numerical expression (linear equation)
        IloLinearNumExpr obj = model.linearNumExpr();
        // Adds terms to the equation
        for(int i = 1; i <= flp.getnFacilities(); i++){
            obj.addTerm(flp.getOpeningCosts()[i-1], x[i-1]);
        }
        for(int i = 1; i<= flp.getnFacilities(); i++){
            for(int j = 1; j <= flp.getnCustomers(); j++){
                obj.addTerm(flp.getDeliveryCosts()[i-1][j-1], y[i-1][j-1]);
            }
        }
        
        // Tells cplex to minimize the objective function
        model.addMinimize(obj);
        
        // 4. Creates the constraints
        
        // Capacity constraints
        for(int i = 1; i<= flp.getnFacilities(); i++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int j = 1; j <= flp.getnCustomers(); j++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            lhs.addTerm(-flp.getCapacity()[i-1], x[i-1]);
            model.addLe(lhs, 0);
        }
        
        // Demand constraints
        for(int j = 1; j <= flp.getnCustomers(); j++){
            IloLinearNumExpr lhs = model.linearNumExpr();
            for(int i = 1; i<= flp.getnFacilities(); i++){
                lhs.addTerm(1, y[i-1][j-1]);
            }
            model.addGe(lhs, flp.getDemands()[j-1]);
        }
        
        
    }
    
    public void solve() throws IloException{
        model.setOut(null);
        model.solve();
        System.out.println("Optimal objective value "+model.getObjValue());
        System.out.println("Optimal solution ");
        for(int i = 1; i<=flp.getnFacilities() ;i++){
            System.out.println("x_"+i+" = "+model.getValue(x[i-1]));
        }
        for(int i = 1; i<= flp.getnFacilities(); i++){
            for(int j = 1; j<=flp.getnCustomers() ;j++){
                System.out.println("y_"+i+","+j+" = "+model.getValue(y[i-1][j-1]));
            }
        }
    }
    
    /**
     * Releases all the objects retained by the IloCplex object.
     * In this particular application it makes no difference.
     * However, in bigger and resource-intensive applications
     * it is advised (if not necessary) to release the resources
     * used by the IloCplex object in order for the program to work well.
     * Note that once the method end() has been called, the IloCplex object
     * cannot be used (e.g., queried) anymore.
     */
    public void end(){
        model.end();
    }
        
}
